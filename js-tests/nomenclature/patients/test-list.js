describe("Patient List", function() {
    beforeEach(module("app"));

    describe('patient controller tests', function () {

        var scope, httpBackend, patientsController;
        var patientList = [ { fn: 'Ventsislav', 'ln': 'Mladenov' } ];

        beforeEach(inject(function ($rootScope, $httpBackend, $controller) {
            httpBackend = $httpBackend;
            httpBackend.expect('GET', '/nomenclature/patients/list').respond(200, patientList);
            scope = $rootScope.$new();
            patientsController = $controller('patientsController', { '$scope': scope });
        }));

        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        it('check patient list methods', function () {
            expect(scope.add).toBeDefined();
            expect(scope.edit).toBeDefined();
            httpBackend.flush();
        });

        it('check patient initialize', function() {
            expect(scope.patients).toEqual([]);
            httpBackend.flush();
            expect(scope.patients).toEqual(patientList);
        });
    });
});