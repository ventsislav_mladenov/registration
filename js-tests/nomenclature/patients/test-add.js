describe("Patient Add/Edit", function() {
    beforeEach(module("app"));

    describe('Patient Add/Edit tests', function () {

        var scope, httpBackend, modalInstance, patientsController;
        var patient = [ { firstName: 'Ventsislav', 'lastName': 'Mladenov' } ];
        var funds = [];

        beforeEach(inject(function ($rootScope, $httpBackend, _$modal_, $controller) {
            httpBackend = $httpBackend;
            httpBackend.expect('GET', '/nomenclature/funds').respond(200, funds);
            httpBackend.expect('GET', '/nomenclature/patients/can_be_deleted?id=0').respond(200, true);
            scope = $rootScope.$new();
            modalInstance = _$modal_.open({
                template: 'a'
            });
            patientsController = $controller('patientAddController', {
                '$scope': scope,
                '$modalInstance': modalInstance,
                'id': 0
            });
        }));

        afterEach(function() {
            modalInstance.dismiss('cancel');
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

//        it('check patient list methods', function () {
//            expect(scope.add).toBeDefined();
//            expect(scope.edit).toBeDefined();
//            httpBackend.flush();
//        });

        it('check patient initialize', function() {
            expect(scope.data).toBeDefined();
            httpBackend.flush();
            expect(scope.data.funds.length).toEqual(1);
        });
    });
});