http_path = "/"
css_dir = "app/static/css"
sass_dir = "app/static/css"
images_dir = "app/static/images"
javascripts_dir = "app/static/js"

# You can select your preferred output style here (can be overridden via the command line):
#output_style = :expanded or :nested or :compact or :compressed
output_style = :expanded