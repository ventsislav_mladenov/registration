﻿/* Drop tables */

DROP TABLE Message_Recipient;    
DROP TABLE Message;
DROP TABLE Doctor_Specialty;
DROP TABLE Specialty;
DROP TABLE Schedule_Date;
DROP TABLE Schedule;
DROP TABLE Patient_Phone;
DROP TABLE Phone_Type;
DROP TABLE Reservation;
DROP TABLE Payment_Info;
DROP TABLE Payment_Type;  
DROP TABLE Patient_Fund_Info;
DROP TABLE Fund;
DROP TABLE Reservation_Lock;
DROP TABLE Reservation_Log;
DROP TABLE Doctor;
DROP TABLE Title;  
DROP TABLE Patient;  
DROP TABLE Gender; 
DROP TABLE Identification_Number_Type;

/* Create tables */

create table Title 
(
  Id SERIAL primary key,
  Abr varchar(10) not null,
  Description varchar(50) not null
);

insert into Title(Abr, Description) values (N'док.', N'Доктор');
insert into Title(Abr, Description) values (N'доц.', N'Доцент');
insert into Title(Abr, Description) values (N'проф.', N'Професор');

create table Specialty
(
  Id SERIAL primary key,
  Description varchar(250) not null
);

insert into Specialty (Description) values(N'Кардиолог');
insert into Specialty (Description) values(N'Пулмолог');
insert into Specialty (Description) values(N'Ортопед');
insert into Specialty (Description) values(N'Уши Нос Гърло');
insert into Specialty (Description) values(N'Дерматолог');
insert into Specialty (Description) values(N'Невролог');

create table Doctor
(
  Id SERIAL primary key,
  Title_Id int not null,
  First_Name varchar(100) not null,
  Last_Name varchar(100) not null,
  Phone_Number varchar(50),
  Default_Exam_Time int default(20) not null,
  constraint Doctor_Title_FK foreign key (Title_Id) references  Title(Id)
);

insert into Doctor(Title_Id, First_Name, Last_Name)
values(2, N'Венцислав', N'Младенов');

create table Doctor_Specialty
(
  Specialty_Id int not null,
  Doctor_Id int not null,
  constraint Doctor_Specialty_PK primary key(Specialty_Id, Doctor_Id),
  constraint Doctor_Specialty_Specialty_FK foreign key (Specialty_Id) references  Specialty(Id) on delete cascade,
  constraint Doctor_Specialty_Doctor_FK foreign key (Doctor_Id) references  Doctor(Id) on delete cascade
);

create table Schedule
(
  Id SERIAL primary key,
  Doctor_Id int not null,
  Date date not null,
  Note varchar(1000),
  constraint Schedule_Doctor_FK foreign key (Doctor_Id) references  Doctor(Id) on delete cascade
);

create table Schedule_Date
(
  Id SERIAL primary key,
  Schedule_Id int not null,
  From_Time time not null,
  To_Time time not null,
  Is_NZOK boolean default(FALSE) not null,
  constraint Schedule_Date_Schedule_FK foreign key (Schedule_Id) references  Schedule(Id) on delete cascade
);

create table Fund
(
  Id SERIAL,
  Name varchar(4000) not null,
  constraint Fund_PK primary key (Id)
);

INSERT INTO Fund (Name) VALUES (N'ДЗИ');
INSERT INTO Fund (Name) VALUES (N'България Здраве');
INSERT INTO Fund (Name) VALUES (N'Медико');

create table Identification_Number_Type
(
  Id INT NOT NULL,
  Type varchar(50) NOT NULL,
  CONSTRAINT Identification_Number_Type_PK PRIMARY KEY (Id)
);

INSERT INTO Identification_Number_Type (Id, Type) VALUES (1, N'ЕГН');
INSERT INTO Identification_Number_Type (Id, Type) VALUES (2, N'ЛНЧ');
INSERT INTO Identification_Number_Type (Id, Type) VALUES (3, N'Частично ЕГН');

create table Gender
(
  Id INT NOT NULL,
  Type varchar(10) NOT NULL,
  CONSTRAINT Gender_PK PRIMARY KEY (Id)
);

INSERT INTO Gender (Id, Type) VALUES (1, N'Мъж');
INSERT INTO Gender (Id, Type) VALUES (2, N'Жена');

create table Patient
(
  Id SERIAL,
  First_Name varchar(100) not null,
  Middle_Name varchar(100) null,
  Last_Name varchar(100) not null,
  Ident_Number varchar(100) null,
  Ident_Number_Type_Id int default(1) not null,
  Gender_Id int default(1) not null,
  Email varchar(100) null,
  Town varchar(100) null,
  Post_Code varchar(50) null,
  Address varchar(300) null,
  Note varchar(4000) null,
  constraint Patient_PK primary key (Id),
  constraint Patient_Identification_Number_Type_FK foreign key (Ident_Number_Type_Id) references Identification_Number_Type(Id),
  constraint Patient_Gender_FK foreign key (Gender_Id) references Gender(Id)
);

create table Patient_Fund_Info
(
  Patient_Id int,
  Fund_Id int not null,
  Fund_Card_Number varchar(50) not null,
  Fund_Card_Expiration date not null,
  constraint Fund_Info_PK primary key (Patient_Id),
  constraint Fund_Info_Fund_FK foreign key (Fund_Id) references Fund(Id),
  constraint Fund_Info_Patient_FK foreign key (Patient_Id) references Patient(Id)
);

CREATE TABLE Phone_Type 
(
  Id INT NOT NULL,
  Type varchar(10) NOT NULL,
  CONSTRAINT Phone_Type_PK PRIMARY KEY (Id)
);

INSERT INTO Phone_Type (Id, Type) VALUES (0, N'Other');
INSERT INTO Phone_Type (Id, Type) VALUES (1, N'Home');
INSERT INTO Phone_Type (Id, Type) VALUES (2, N'Work');
INSERT INTO Phone_Type (Id, Type) VALUES (3, N'Mobile');

create table Patient_Phone
(
  Id SERIAL,
  Patient_Id int not null,
  Number varchar(50) not null,
  Type_Id int not null default(0),
  Is_Primary boolean not null default (FALSE),  
  constraint Patient_Phone_PK primary key (Id),
  constraint Patient_Phone_Patient_FK foreign key (Patient_Id) references Patient(Id) on delete cascade,
  constraint Patient_Phone_Phone_Type_FK foreign key (Type_Id) references Phone_Type(Id) on delete set default
);

CREATE TABLE Payment_Type 
(
  Id int not null,
  Type varchar(100) not null,
  constraint Payment_Type_PK primary key (Id)
);

insert into Payment_Type (Id, Type) values (1, N'National Fund');
insert into Payment_Type (Id, Type) values (2, N'Paid');
insert into Payment_Type (Id, Type) values (3, N'Private Fund');

create table Payment_Info
(
  Id SERIAL,
  Fund_Id int null,
  Fund_Card_Number varchar(50) null,
  Fund_Card_Expiration date null,
  constraint Payment_Info_PK primary key (Id),
  constraint Payment_Info_Fund_FK foreign key (Fund_Id) references Fund(Id)
);

create table Reservation
(
  Id SERIAL,
  Patient_Id int not null,
  Doctor_Id int not null,
  Date date not null,
  From_Time time not null,
  To_Time time not null,
  Payment_Type_Id int not null,
  Payment_Info_Id int null,
  Note varchar(4000) null,
  Created_By int not null,
  constraint Reservation_PK primary key (Id),
  constraint Reservation_Patient_FK foreign key (Patient_Id) references Patient(Id),
  constraint Reservation_Doctor_FK foreign key (Doctor_Id) references Doctor(Id),
  constraint Reservation_Payment_Type_FK foreign key (Payment_Type_Id) references Payment_Type(Id),
  constraint Reservation_Payment_Info_FK foreign key (Payment_Info_Id) references Payment_Info(Id),
  constraint Reservation_User_FK foreign key (Created_By) references auth_user(id)
);

create table Reservation_Lock
(
  Id SERIAL,
  User_Id int not null,
  Doctor_Id int not null,
  Lock_Time timestamp default(current_timestamp) not null,
  Date date not null,
  From_Time time not null,
  To_Time time not null,
  constraint Reservation_Lock_PK primary key (Id),
  constraint Reservation_Lock_User_Fk foreign key (User_Id) references auth_user(Id),
  constraint Reservation_Lock_Doctor_Fk foreign key (User_Id) references Doctor(Id)
);

create table Reservation_Log
(
  Id SERIAL,
  User_Id int not null,
  Log_Time timestamp default(current_timestamp) not null,
  Operation_Type int not null,
  Patient_Id int not null,
  Doctor_Id int not null,
  Date date not null,
  From_Time time not null,
  To_Time time not null,
  constraint Reservation_Log_PK primary key (Id),
  constraint Reservation_Log_User_Fk foreign key (User_Id) references auth_user(Id),
  constraint Reservation_Log_Doctor_Fk foreign key (User_Id) references Doctor(Id),
  constraint Reservation_Log_Patient_Fk foreign key (Patient_Id) references Patient(Id)
);

create table Message
(
  Id SERIAL,
  User_Id int not null,
  Text varchar(4000) not null,
  /*
  1 - Success - Green
  2 - Info - blue
  3 - Warning - Yellow
  4 - Error - Red
  */
  Type int not null,
  Send_At timestamp default(current_timestamp) not null,
  /*
  1 - One time
  2 - Interval
  */
  Validity int not null,
  Valid_From date null,
  Valid_To date null,
  constraint Message_PK primary key (Id),
  constraint Message_Type_CK check(Type in (1, 2, 3, 4)), 
  constraint Message_Validity_CK check(Validity in (1, 2)), 
  constraint Message_User_FK foreign key (User_Id) references auth_user(Id)
);

create table Message_Recipient 
(
  Id SERIAL,
  Message_Id int not null,
  User_Id int not null,
  Read_At timestamp null,
  constraint Message_Recipient_PK primary key (Id),
  constraint Message_Recipient_Message_FK foreign key (Message_Id) references Message(Id),
  constraint Message_Recipient_User_FK foreign key (User_Id) references auth_user(Id)
);