from django.contrib.auth.models import Group, User
from django.utils import unittest
from django.test import TestCase
__author__ = 'vmladenov'


class RoleUserTests(TestCase):

    def setUp(self):
        User.objects.create_user('test', 'test@test.com', '123', first_name='v', last_name='m')
        Group.objects.create(name='test_group')

    def test_assign_role(self):
        user = User.objects.get(username='test')
        self.assertIsNotNone(user)
        role = Group.objects.get(name='test_group')
        self.assertIsNotNone(role)
        user.groups.add(role)
        user.save()

        user = User.objects.get(first_name='v', last_name='m')
        roles = user.groups.all()
        self.assertGreater(len(roles), 0)
        self.assertEqual(roles[0].id, 1)


