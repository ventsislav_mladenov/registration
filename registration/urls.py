from django.conf.urls import patterns, include, url
from django.contrib import admin

from app.account.views import Login

admin.autodiscover()

urlpatterns = patterns(
    '',
    #home
    url(r'^$', 'app.views.home', name='home'),
    url(r'^$', 'app.views.home', name='get_messages'),
    # account
    url(r'^login$', Login.as_view(), name='login'),
    url(r'^logout', 'app.account.views.logoff', name='logout'),
    # nomenclature
    url(r'^nomenclature/', include('app.nomenclature')),

    url(r'^admin/', include(admin.site.urls)),
)
