from django.contrib.auth.models import User
from django.db import models


class Specialty(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=250)

    class Meta:
        managed = False
        ordering = ['id']
        db_table = 'specialty'


class Doctor(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.ForeignKey('Title')
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=50, blank=True)
    default_exam_time = models.IntegerField()
    specialties = models.ManyToManyField(Specialty, db_table='doctor_specialty')

    class Meta:
        managed = False
        db_table = 'doctor'


class Fund(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=4000)

    class Meta:
        managed = False
        db_table = 'fund'


class Gender(models.Model):
    id = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'gender'


class IdentificationNumberType(models.Model):
    id = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'identification_number_type'


class Message(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User)
    text = models.CharField(max_length=4000)
    type = models.IntegerField()
    send_at = models.DateTimeField()
    validity = models.IntegerField()
    valid_from = models.DateField(blank=True, null=True)
    valid_to = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'message'


class MessageRecipient(models.Model):
    id = models.IntegerField(primary_key=True)
    message = models.ForeignKey(Message)
    user = models.ForeignKey(User)
    read_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'message_recipient'


class Patient(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100)
    ident_number = models.CharField(max_length=100, blank=True)
    ident_number_type = models.ForeignKey(IdentificationNumberType)
    gender = models.ForeignKey(Gender)
    email = models.CharField(max_length=100, blank=True)
    town = models.CharField(max_length=100, blank=True)
    post_code = models.CharField(max_length=50, blank=True)
    address = models.CharField(max_length=300, blank=True)
    note = models.CharField(max_length=4000, blank=True)

    class Meta:
        managed = False
        db_table = 'patient'


class PatientFundInfo(models.Model):
    patient = models.OneToOneField(Patient, primary_key=True, related_name='patientFundInfo')
    fund = models.ForeignKey(Fund)
    fund_card_number = models.CharField(max_length=50)
    fund_card_expiration = models.DateField()

    class Meta:
        managed = False
        db_table = 'patient_fund_info'


class PatientPhone(models.Model):
    id = models.AutoField(primary_key=True)
    patient = models.ForeignKey(Patient)
    number = models.CharField(max_length=50)
    type = models.ForeignKey('PhoneType')
    is_primary = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'patient_phone'


class PaymentInfo(models.Model):
    id = models.AutoField(primary_key=True)
    fund = models.ForeignKey(Fund, blank=True, null=True)
    fund_card_number = models.CharField(max_length=50, blank=True)
    fund_card_expiration = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'payment_info'


class PaymentType(models.Model):
    id = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'payment_type'


class PhoneType(models.Model):
    id = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'phone_type'


class Reservation(models.Model):
    id = models.IntegerField(primary_key=True)
    patient = models.ForeignKey(Patient)
    doctor = models.ForeignKey(Doctor)
    date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()
    payment_type = models.ForeignKey(PaymentType)
    payment_info = models.ForeignKey(PaymentInfo, blank=True, null=True)
    note = models.CharField(max_length=4000, blank=True)
    created_by = models.ForeignKey(User, db_column='created_by')

    class Meta:
        managed = False
        db_table = 'reservation'


class ReservationLock(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(Doctor)
    doctor_id = models.IntegerField()
    lock_time = models.DateTimeField()
    date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()

    class Meta:
        managed = False
        db_table = 'reservation_lock'


class ReservationLog(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(Doctor)
    log_time = models.DateTimeField()
    operation_type = models.IntegerField()
    patient = models.ForeignKey(Patient)
    doctor_id = models.IntegerField()
    date = models.DateField()
    from_time = models.TimeField()
    to_time = models.TimeField()

    class Meta:
        managed = False
        db_table = 'reservation_log'


# class Role(models.Model):
#     id = models.IntegerField(primary_key=True)
#     name = models.CharField(max_length=100)
#     users = models.ManyToManyField('User', db_table='role_user')
#
#     class Meta:
#         managed = False
#         db_table = 'role'


class Schedule(models.Model):
    id = models.IntegerField(primary_key=True)
    doctor = models.ForeignKey(Doctor)
    date = models.DateField()
    note = models.CharField(max_length=1000, blank=True)

    class Meta:
        managed = False
        db_table = 'schedule'


class ScheduleDate(models.Model):
    id = models.IntegerField(primary_key=True)
    schedule = models.ForeignKey(Schedule)
    from_time = models.TimeField()
    to_time = models.TimeField()
    is_nzok = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'schedule_date'


class Title(models.Model):
    id = models.IntegerField(primary_key=True, )
    abr = models.CharField(max_length=10)
    description = models.CharField(max_length=50)

    class Meta:
        managed = False
        ordering = ['id']
        db_table = 'title'


# class User(models.Model):
#     id = models.AutoField(primary_key=True)
#     user_name = models.CharField(max_length=100)
#     password = models.CharField(max_length=4000)
#     salt = models.CharField(max_length=4000)
#     first_name = models.CharField(max_length=100)
#     last_name = models.CharField(max_length=100)
#     email = models.CharField(max_length=100)
#
#     class Meta:
#         managed = False
#         db_table = 'user'
