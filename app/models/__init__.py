from types import MethodType
from django.contrib.auth.models import User


def in_group(self, group_name):
    return self.groups.filter(name__iexact=group_name).exists()

User.in_group = in_group