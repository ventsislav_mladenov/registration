from django.conf.urls import url, include, patterns

urlpatterns = patterns(
    '',
    url(r'^', include('app.nomenclature.common')),
    url(r'^doctors/', include('app.nomenclature.doctors')),
    url(r'^patients/', include('app.nomenclature.patients')),
)
