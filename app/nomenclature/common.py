from django.conf.urls import url
from app.models.models import Title, Specialty, Fund
from app.utils import json_response


def get_titles(request):
    titles = Title.objects.all()
    data = [{
                'id': title.id,
                'abr': title.abr,
                'description': title.description
            } for title in titles]
    return json_response(data)


def get_specialties(request):
    specialties = Specialty.objects.all()
    data = [{
            'id': specialty.id,
            'description': specialty.description
            } for specialty in specialties]
    return json_response(data)


def get_funds(request):
    funds = Fund.objects.all()
    data = [{
            'id': fund.id,
            'name': fund.name
            } for fund in funds]
    return json_response(data)

urlpatterns = [
    url(r"^titles$", get_titles),
    url(r"^specialties$", get_specialties),
    url(r"^funds$", get_funds)
]