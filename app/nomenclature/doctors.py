from django.conf.urls import url
from django.shortcuts import render
from app.models.models import Doctor, Reservation, Specialty
from app.utils import json_response


def get_doctors_view(request):
    return render(request, "nomenclature/doctors/list.html")


def get_doctors(request):
    doctors = Doctor.objects.all()
    data = [{
            'id': doc.id,
            'title': doc.title.description,
            'phoneNumber': doc.phone_number,
            'firstName': doc.first_name,
            'lastName': doc.last_name
            } for doc in doctors]
    return json_response(data)


def can_be_deleted(request, doctor_id):
    has_reservations = Reservation.objects.filter(doctor__id=doctor_id).exists() > 0
    return json_response(request.user.in_group('admin') and not has_reservations)


def get_doctor(request, doctor_id):
    doctor = Doctor.objects.get(id=doctor_id)
    return json_response({
        'titleId': doctor.title_id,
        'firstName': doctor.first_name,
        'lastName': doctor.last_name,
        'phoneNumber': doctor.phone_number,
        'defaultExamTime': doctor.default_exam_time,
        'specialties': [{
                        'id': sp.id,
                        'description': sp.description
                        } for sp in doctor.specialties.all()]
    })


def add_doctor(request):
    return render(request, 'nomenclature/doctors/add.html')


def save_doctor(request):
    import json

    doctor_specialties = json.loads(request.body.decode())

    def insert_doctor(doctor, specialties):
        new_doctor = Doctor.objects.create(title_id=doctor['titleId'],
                                           first_name=doctor['firstName'],
                                           last_name=doctor['lastName'],
                                           phone_number=doctor['phoneNumber'],
                                           default_exam_time=doctor['defaultExamTime'])
        for specialty in specialties:
            existing_specialty = Specialty.objects.get(id=specialty['id'])
            new_doctor.specialties.add(existing_specialty)
        new_doctor.save()

    def update_doctor(doctor, specialties):
        existing_doctor = Doctor.objects.get(id=doctor['id'])
        existing_doctor.title_id = doctor['titleId']
        existing_doctor.first_name = doctor['firstName']
        existing_doctor.last_name = doctor['lastName']
        existing_doctor.phone_number = doctor['phoneNumber']
        existing_doctor.default_exam_time = doctor['defaultExamTime']
        existing_doctor.specialties.clear()
        for specialty in specialties:
            existing_specialty = Specialty.objects.get(id=specialty['id'])
            existing_doctor.specialties.add(existing_specialty)
        existing_doctor.save()

    doctor = doctor_specialties['doctor']
    specialties = doctor_specialties['specialties']
    if doctor['id'] == 0:
        insert_doctor(doctor, specialties)
    else:
        update_doctor(doctor, specialties)
    return json_response(1)


urlpatterns = [
    url(r'^$', get_doctors_view, name='doctors'),
    url(r"^list$", get_doctors),
    url(r"^can_be_deleted/(?P<doctor_id>[0-9]+)", can_be_deleted),
    url(r"^get/(?P<doctor_id>[0-9]+)", get_doctor),
    url(r"^add$", add_doctor),
    url(r"^save$", save_doctor)
]