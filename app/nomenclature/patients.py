from django.conf.urls import url
from django.shortcuts import render
from app.infrastructure.exceptions import WrongParameterValue, MissingParameter
from app.infrastructure.helpers import check_relation
from app.models.models import Patient, Reservation, IdentificationNumberType, PatientPhone, PatientFundInfo
from app.utils import json_response


def get_patients_view(request):
    return render(request, "nomenclature/patients/list.html")


def get_patients(request):
    patients = Patient.objects.all()
    data = [{
            'id': p.id,
            'fn': p.first_name,
            'ln': p.last_name,
            'in': p.ident_number,
            'pn': p.patientphone_set.filter(is_primary=True).first().number,
            'em': p.email
            } for p in patients]
    return json_response(data)


def add_patient(request):
    return render(request, 'nomenclature/patients/add.html')


def can_be_deleted(request):
    if 'id' in request.GET:
        patient_id = int(request.GET['id'])
        if patient_id <= 0:
            # new patient
            return json_response(False)
        has_reservations = Reservation.objects.filter(patient__id=patient_id).exists() > 0
        return json_response(not has_reservations)


def save_patient(request):
    import json
    patient = json.loads(request.body.decode())

    def insert_phones(patient, json_patient):
        for phone in json_patient['patientPhones']:
            patient.patientphone_set.add(PatientPhone(patient=patient,
                                                      number=phone['number'],
                                                      type_id=phone['typeId'],
                                                      is_primary=phone['isPrimary']))

    def set_fund_info(patient, json_patient):
        if 'patientFundInfo' in json_patient:
            if not check_relation(patient, 'patientFundInfo'):
                patient.patientfundinfo = PatientFundInfo.objects.create(
                    patient=patient,
                    fund_id=json_patient['patientFundInfo']['fundId'],
                    fund_card_number=json_patient['patientFundInfo']['fundCardNumber'],
                    fund_card_expiration=json_patient['patientFundInfo']['fundCardExpiration']
                )
            else:
                patient.patientfundinfo.fund_id = json_patient['patientFundInfo']['fundId']
                patient.fund_card_number=json_patient['patientFundInfo']['fundCardNumber']
                patient.fund_card_expiration=json_patient['patientFundInfo']['fundCardExpiration']
        else:
            if not(patient.patientfundinfo is None):
                patient.patientfundinfo.delete()

    def insert(patient):
        new_patient = Patient.objects.create(first_name=patient['firstName'],
                                             middle_name=patient['middleName'],
                                             last_name=patient['lastName'],
                                             ident_number=patient['identNumber'],
                                             ident_number_type_id=patient['identNumberTypeId'],
                                             gender_id=patient['genderId'],
                                             email=patient['email'],
                                             address=patient['address'],
                                             town=patient['town'],
                                             post_code=patient['postCode'],
                                             note=patient['note'])
        insert_phones(new_patient, patient)
        set_fund_info(new_patient, patient)
        new_patient.save()

    def update(patient):
        existing_patient = Patient.objects.get(id=patient['id'])
        existing_patient.first_name = patient['firstName']
        existing_patient.middle_name = patient['middleName']
        existing_patient.last_name = patient['lastName']
        existing_patient.ident_number = patient['identNumber']
        existing_patient.ident_number_type_id = patient['identNumberTypeId']
        existing_patient.gender_id = patient['genderId']
        existing_patient.email = patient['email']
        existing_patient.address = patient['address']
        existing_patient.town = patient['town']
        existing_patient.post_code = patient['postCode']
        existing_patient.note = patient['note']

        for phone in existing_patient.patientphone_set.all():
            phone.delete()
        #existing_patient.patientphone_set.clear()
        insert_phones(existing_patient, patient)
        set_fund_info(existing_patient, patient)
        existing_patient.save()

    if not('id' in patient) or patient['id'] == 0:
        insert(patient)
    else:
        update(patient)
    return json_response(1)


def get_patient(request):
    if 'id' in request.GET:
        patient_id = int(request.GET['id'])
        if patient_id <= 0:
            raise WrongParameterValue('id')
        patient = Patient.objects.get(id=patient_id)
        return json_response({
            'firstName': patient.first_name,
            'middleName': patient.middle_name,
            'lastName': patient.last_name,
            'identNumberTypeId': patient.ident_number_type_id,
            'identNumber': patient.ident_number,
            'genderId': patient.gender_id,
            'email': patient.email,
            'address': patient.address,
            'town': patient.town,
            'postCode': patient.post_code,
            'note': patient.note,
            'patientPhones': [{
                              'typeId': pp.type_id,
                              'number': pp.number,
                              'isPrimary': pp.is_primary
                              } for pp in patient.patientphone_set.all()]
        })
    else:
        raise MissingParameter('id')

urlpatterns = [
    url(r'^$', get_patients_view, name='patients'),
    url(r"^list$", get_patients),
    url(r"^can_be_deleted", can_be_deleted),
    url(r"^patient", get_patient),
    url(r"^add$", add_patient),
    url(r"^save$", save_patient)
]