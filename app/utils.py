import codecs

__author__ = 'vmladenov'
from django.test.runner import DiscoverRunner


class MyDiscoverRunner(DiscoverRunner):
    def setup_databases(self, **kwargs):
        databases = super(MyDiscoverRunner, self).setup_databases(**kwargs)

        import os

        sql_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'DB.sql')
        with open(sql_file) as f:
            # read file and remove UTF-8 BOM
            sql = f.read().encode().lstrip(codecs.BOM_UTF8).decode()
            statements = sql.split(';')
            import re

            p = re.compile('\\/\\*.+\\*\\/')
            f.close()

        db, mirrors = databases
        for connection, name, destroy in db:
            cursor = connection.cursor()
            for statement in statements:
                statement = p.sub('', statement).strip()
                if statement.startswith('DROP') or statement == '':
                    continue
                cursor.execute(statement)
            cursor.close()
        return databases


from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from re import compile

EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware:
    def process_request(self, request):
        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect(settings.LOGIN_URL)


def main_context_processor(request):
    from django.conf import settings

    lang = 'en'
    if 'lang' in request.COOKIES:
        lang = request.COOKIES['lang']
    return {
        'version': settings.VERSION,
        'lang': lang
    }


def json_response(data):
    import json
    json_data = json.dumps(data, ensure_ascii=False)
    return HttpResponse(json_data, content_type="application/json; charset=utf-8")
