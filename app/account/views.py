__author__ = 'vmladenov'
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.views.generic.base import View


class Login(View):
    def get(self, request):
        return render(request, "account/login.html")

    def post(self, request):
        username = request.POST.get('userName', '')
        password = request.POST.get('password', '')
        remember_me = request.POST.get('remember_me', 'off')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            if remember_me:
                request.session.set_expiry(2 * 7 * 24 * 60 * 60)  # Two weeks
            return redirect("home")
        return render(request, "account/login.html", {'error': 'Wrong user or password!'})


def logoff(request):
    logout(request)
    return redirect("login")