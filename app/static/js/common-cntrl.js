﻿var cntrl = angular.module('common.cntrl', []);
cntrl.controller('userSelectController', [
    '$scope', '$http', '$modalInstance', function ($scope, $http, $modalInstance) {
        $scope.users = [];
        var __self = this;

        __self.loadUsers = function () {
            $http({
                method: 'GET',
                url: '/Configuration/UserAdministration/GetUsers',
            }).success(function (users) {
                $scope.users.clear();
                $scope.users.pushAll(_.map(users, function (d) {
                    d.selected = false;
                    return d;
                }));
            });
        };

        __self.loadUsers();

        $scope.ok = function () {
            var selected = [];
            for (var i = 0; i < $scope.users.length; i++) {
                if ($scope.users[i].selected)
                    selected.push($scope.users[i]);
            }
            $modalInstance.close(selected);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);

cntrl.factory('userSelect', ['$modal', function ($modal) {
    return {
        selectUsers: function () {
            var addRecipientInstance = $modal.open({
                templateUrl: '/Configuration/UserAdministration/SelectUsers',
                controller: 'userSelectController',
            });
            return addRecipientInstance.result;
        }
    };
}]);
