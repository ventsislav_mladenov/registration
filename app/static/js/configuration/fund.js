﻿app.controller('fundController', [
    '$scope', '$http', function ($scope, $http) {
        $scope.funds = [];
        $scope.error = '';
        var __self = this;

        __self.refresh = function () {
            $scope.funds.clear();
            $http({
                method: 'GET',
                url: '/Configuration/Fund/GetFunds'
            }).success(function (funds) {
                $scope.funds.pushAll(_.map(funds, function (f) {
                    f.originName = f.name;
                    f.readOnly = true;
                    return f;
                }));
            });
        }

        __self.refresh();


        $scope.edit = function (fund) {
            fund.readOnly = false;
        };

        $scope.save = function (fund) {
            $scope.error = '';
            if (!fund.name) {
                $scope.error = 'Моля попълнете име.';
                return;
            }
            $http({
                method: 'POST',
                url: '/Configuration/Fund/Save',
                data: {
                    id: fund.id,
                    name: fund.name
                }
            }).success(function () { __self.refresh(); });
        };

        $scope.add = function () {
            $scope.funds.push({
                id: 0,
                name: '',
                readOnly: false
            });
        };

        $scope.cancel = function (fund) {
            if (fund.id > 0) {
                fund.name = fund.originName;
                fund.readOnly = true;
            } else {
                $scope.funds.remove(fund);
            }
        };

        $scope.del = function (id) {
            $scope.error = '';
            if (confirm('Сигурни ли сте че искате да изтриете фонда?')) {
                $http({
                    method: 'POST',
                    url: '/Configuration/Fund/Delete',
                    data: {
                        id: id
                    }
                }).success(function(res) {
                    if (res.msg) {
                        $scope.error = res.msg;
                    } else {
                        __self.refresh();
                    }
                });
            }
        };
    }
]);