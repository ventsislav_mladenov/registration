﻿app.controller('messageController', [
    '$scope', '$http', 'dateHelper', 'userSelect', function ($scope, $http, dateHelper, userSelect) {

        $scope.errors = [];
        $scope.types = [
            { id: 2, text: 'Информативно' },
            { id: 1, text: 'Добро' },
            { id: 3, text: 'Внимание' },
            { id: 4, text: 'Грешка' }
        ];
        $scope.validities = [
            { id: 1, text: 'Един път' },
            { id: 2, text: 'Интервал' }
        ];
        $scope.view = 1;
        $scope.messages = [];

        var __self = this;
        __self.init = function() {
            return {
                text: '',
                type: $scope.types.find(function(t) { return t.id == 2; }),
                validity: $scope.validities.find(function(v) { return v.id == 1; }),
                from: dateHelper.copyDate(new Date()),
                to: null,
                recipients: []
            };
        };

        $scope.message = __self.init();

        $scope.addRecipients = function () {
            userSelect.selectUsers().then(function (users) {
                $scope.message.recipients.clear();
                $scope.message.recipients.pushAll(users);
            });
        };

        $scope.deleteRecipient = function (recipient) {
            $scope.message.recipients.remove(recipient);
        };

        $scope.send = function () {
            $scope.errors.clear();
            if ($scope.message.recipients.length == 0) {
                $scope.errors.push('Моля добавете получатели');
            }
            if (!$scope.message.text) {
                $scope.errors.push('Моля въведете съобщение');
            }
            if ($scope.message.validity.id === 2 && !$scope.message.from) {
                $scope.errors.push('Моля въведете начална дата');
            }
            if ($scope.errors.length > 0)
                return;

            var message = {
                text: $scope.message.text,
                type: $scope.message.type.id,
                validity: $scope.message.validity.id,
                validFrom: dateHelper.dateToString($scope.message.from),
                validTo: dateHelper.dateToString($scope.message.to),
                recipients: _.map($scope.message.recipients, function (r) {
                    return {
                        userId: r.id
                    };
                })
            };

            if (message.validity == 1) {
                message.validFrom = null;
                message.validTo = null;
            }

            $http(
            {
                method: 'POST',
                url: '/Configuration/Messages/Send',
                data: message
            }).success(function (res) {
                if (res.result === 1) {
                    alert('Съобщението е изпратено');
                    $scope.message = __self.init();
                }
                if (res.msg) {
                    $scope.errors.push(res.msg);
                }
            });
        };

        __self.loadMessages = function () {
            $http(
            {
                method: 'GET',
                url: '/Configuration/Messages/GetMessages',
            }).success(function (res) {
                $scope.messages.clear();
                $scope.messages.pushAll(res);
            });
        };


        $scope.getTypeText = function(id) {
            return $scope.types.find(function(t) { return t.id == id; }).text;
        };

        $scope.getValidityText = function (id) {
            return $scope.validities.find(function (v) { return v.id == id; }).text;
        };

        $scope.resend = function(message) {
            $scope.message = __self.init();
            $scope.message.text = message.text;
            $scope.message.type = $scope.types.find(function (t) { return t.id == message.type; });
            $scope.message.validity = $scope.validities.find(function (v) { return v.id == message.validity; });
            $scope.message.from = dateHelper.parseDate(message.validFrom);
            $scope.message.to = dateHelper.parseDate(message.validTo);
            $scope.message.recipients.pushAll(message.recipients);
            $scope.view = 1;
        };

        $scope.$watch('view', function(newVal, oldVal) {
            if (newVal == 2) {
                __self.loadMessages();
            }
        });
    }
]);

app.filter('userCompact', function() {
    return function(users) {
        var res = '';
        if (!users)
            return res;
        for (var i = 0; i < users.length; i++) {
            res += users[i].firstName + ' ' + users[i].lastName + ', ';
        }
        return res.substr(0, res.length - 2);
    };
});