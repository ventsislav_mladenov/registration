﻿app.controller("doctorsController", ["$scope", '$http', "$modal", "$rootScope", function ($scope, $http, $modal, $rootScope) {
    var _self = this;
    $scope.doctors = [];

    _self.loadDoctors = function () {
        $http({
            method: 'GET',
            url: '/nomenclature/doctors/list'
        }).success(function (doctors) {
            $scope.doctors = doctors;
        });
    };

    _self.loadDoctors();


    _self.openDoctor = function (id) {
        var addDoctorInstance = $modal.open({
            templateUrl: '/nomenclature/doctors/add',
            controller: 'doctorAddController',
            resolve: {
                id: function () {
                    return id;
                }
            }
        });
        addDoctorInstance.result.then(function () {
            _self.loadDoctors();
        });
    };

    $scope.edit = function (id) {
        _self.openDoctor(id);
    };

    $scope.add = function () {
        _self.openDoctor(0);
    };
}]);

app.controller('doctorAddController', [
    '$scope', '$http', '$q', '$modalInstance', 'id', function ($scope, $http, $q, $modalInstance, id) {
        var __self = this;
        $scope.data = {
            titles: [],
            specialties: [
                { id: 0, description: $scope.translation.texts.selectSpecialty }
            ]
        };


        $scope.model = {
            id: id,
            title: {},
            firstName: '',
            lastName: '',
            phoneNumber: '',
            defaultExamTime: 20,
            specialties: [],
            errors: []
        };

        __self.loadTitles = function () {
            return $http({
                method: 'GET',
                url: '/nomenclature/titles'
            }).success(function (titles) {
                $scope.data.titles.pushAll(titles);
                $scope.model.title = titles[0];
            });
        };

        __self.loadSpecialties = function () {
            return $http({
                method: 'GET',
                url: '/nomenclature/specialties'
            }).success(function (specialties) {
                $scope.data.specialties.pushAll(specialties);
            });
        };

        __self.getCanBeDeleted = function () {
            return $http({
                method: 'GET',
                url: '/nomenclature/doctors/can_be_deleted/' + id
            }).success(function (res) {
                $scope.model.canBeDeleted = res;
            });
        };

        __self.loadData = function () {
            var waits = [];
            waits.push(__self.loadTitles());
            waits.push(__self.loadSpecialties());
            waits.push(__self.getCanBeDeleted());
            return $q.all(waits);
        };

        __self.loadDoctor = function () {
            $http({
                method: 'GET',
                url: '/nomenclature/doctors/get/' + id
            }).success(function (doctor) {
                $scope.model.title = $scope.data.titles.find(function (t) { return t.id === doctor.titleId; });
                $scope.model.firstName = doctor.firstName;
                $scope.model.lastName = doctor.lastName;
                $scope.model.phoneNumber = doctor.phoneNumber;
                $scope.model.defaultExamTime = doctor.defaultExamTime;

                for (var i = 0; i < doctor.specialties.length; i++) {
                    var specialty = doctor.specialties[i];
                    $scope.model.specialties.push({
                        type: $scope.data.specialties.find(function(x) { return x.id == specialty.id; }),
                        types: _.filter($scope.data.specialties, function (s) {
                            if (s.id === 0)
                                return true;
                            return !_.any($scope.model.specialties, function (ms) { return ms.type.id === s.id; });
                        })
                    });
                }
            });
        };

        __self.loadData().then(function () {
            if (id > 0) {
                __self.loadDoctor();
            }
        });

        $scope.save = function () {
            $scope.model.errors.clear();
            if (!$scope.model.firstName) {
                $scope.model.errors.push($scope.translation.texts.pleaseEnterFirstName);
            }
            if (!$scope.model.lastName) {
                $scope.model.errors.push($scope.translation.texts.pleaseEnterLastName);
            }
            if ($scope.model.errors.length > 0)
                return false;

            var doctor = {
                id: $scope.model.id,
                titleId: $scope.model.title.id,
                firstName: $scope.model.firstName,
                lastName: $scope.model.lastName,
                phoneNumber: $scope.model.phoneNumber,
                defaultExamTime: $scope.model.defaultExamTime
            };
            var specialties = [];
            for (var i = 0; i < $scope.model.specialties.length; i++) {
                var speciality = $scope.model.specialties[i];
                if (speciality.type.id > 0)
                    specialties.push(speciality.type);
            }
            $http(
            {
                method: 'POST',
                url: '/nomenclature/doctors/save',
                data: {
                    doctor: doctor,
                    specialties: specialties
                }
            }).success(function (res) {
                __self.ok();
            }).error(function (err) {
                $scope.model.errors.push(err);
            });

            return true;
        };

        __self.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.del = function () {
            if (confirm($scope.translation.texts.areYouSureWantDeleteDoctor + ': ' + $scope.model.firstName + ' ' + $scope.model.lastName)) {
                $http(
                {
                    method: 'POST',
                    url: '/nomenclature/doctors/delete',
                    data: {
                        id: $scope.model.id
                    }
                }).success(function (res) {
                    __self.ok();
                }).error(function (err) {
                    $scope.model.errors.push(err);
                });
            }
        };

        $scope.addNewSpeciality = function () {
            if (_.any($scope.model.specialties, function (s) { return s.type.id === 0; }))
                return;
            $scope.model.specialties.push({
                type: $scope.data.specialties[0],
                types: _.filter($scope.data.specialties, function (s) {
                    if (s.id === 0)
                        return true;
                    return !_.any($scope.model.specialties, function (ms) { return ms.type.id === s.id; });
                })
            });
        };

        $scope.removeSpeciality = function (speciality) {
            $scope.model.specialties.remove(speciality);
        };
    }
]);