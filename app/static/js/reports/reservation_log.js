﻿(
    function ($) {

        var dataFormatResult = function (data) {
            return data.firstName + ' ' + data.lastName;
        };

        var dataFormatSelection = function (data) {
            return data.firstName + ' ' + data.lastName;
        };

        $("#userSelect").select2();
        $("#doctorSelect").select2();
        $("#patientSelect").select2({
            placeholder: "Търсене на пациент",
            minimumInputLength: 2,
            ajax: {
                url: "/Reports/ReservationLog/SearchPatients",
                dataType: 'json',
                quietMillis: 100,
                data: function (term) {
                    return {
                        search: term
                    };
                },
                results: function (data) {
                    return { results: data };
                }
            },
            initSelection: function (element, callback) {
                var id = parseInt($(element).val());
                if (!isNaN(id)) {
                    $.ajax("/Reports/ReservationLog/GetPatient?id=" + id, {
                        dataType: "json"
                    }).done(function (data) { callback(data); });
                } else {
                    callback(
                    {
                        id: 0,
                        firstName: 'Търсене',
                        lastName: 'на пациент'
                    });
                }
            },
            formatResult: dataFormatResult,
            formatSelection: dataFormatSelection
        });
    }
)(jQuery);
