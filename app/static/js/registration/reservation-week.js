﻿app.factory('week', ['$http', '$q', 'dateHelper', 'timeConverter', function ($http, $q, dateHelper, timeConverter) {
    var __self = this;
    __self.days = [];
    __self.reservations = [];

    __self.setWeekByDate = function (date) {
        __self.from = dateHelper.copyDate(date);
        __self.from.setDate(__self.from.getDate() - (7 + __self.from.getDay() - 1) % 7);
        __self.to = dateHelper.copyDate(__self.from);
        __self.to.setDate(__self.to.getDate() + 6);
    };

    __self.buildHours = function (dayMinHour, dayMaxHour, doctor) {
        var sortedSchedules = _.sortBy(doctor.schedule, function (x) { return x.fromTime; });
        doctor.hours = [];

        for (var i = 0; i < sortedSchedules.length; i++) {
            var schedule = sortedSchedules[i];
            if (schedule.toTime === 1439)
                schedule.toTime = 1440;
            if (i == 0) {
                doctor.hours.push({
                    work: 0,
                    from: dayMinHour,
                    to: schedule.fromTime
                });
            } else {
                doctor.hours.push({
                    work: 0,
                    from: sortedSchedules[i - 1].toTime,
                    to: schedule.fromTime
                });
            }

            var time = schedule.fromTime + doctor.examTime;
            while (time < schedule.toTime) {
                doctor.hours.push({
                    work: 1,
                    from: time - doctor.examTime,
                    to: time,
                    isnzok: schedule.isnzok
                });
                time += doctor.examTime;
            }
            if (time - doctor.examTime < schedule.toTime) {
                doctor.hours.push({
                    work: 1,
                    from: time - doctor.examTime,
                    to: schedule.toTime,
                    isnzok: schedule.isnzok
                });
            }

            if (i == sortedSchedules.length - 1) {
                doctor.hours.push({
                    work: 0,
                    from: schedule.toTime,
                    to: dayMaxHour
                });
            }
        }

    };

    __self.getSchedule = function () {
        __self.days.clear();
        return $http({
            method: 'GET',
            url: '/Reservation/Reservation/GetDoctorsSchedule',
            params: {
                fromDate: dateHelper.dateToString(__self.from),
                toDate: dateHelper.dateToString(__self.to)
            }
        })
        .success(function (data) {
            for (var i = 0; i < data.length; i++) {
                var schedule = data[i];
                var hours = [];
                for (var k = schedule.dayMinHour; k < schedule.dayMaxHour; k += 60) {
                    hours.push(k / 60);
                }

                for (var j = 0; j < schedule.doctors.length; j++) {
                    var doctor = schedule.doctors[j];
                    __self.buildHours(schedule.dayMinHour, schedule.dayMaxHour, doctor);
                }
                __self.days.push({
                    date: dateHelper.parseDate(schedule.date),
                    hours: hours,
                    doctors: schedule.doctors
                });
            }
        });
    };

    __self.getAllReservations = function () {
        __self.reservations.clear();
        return $http({
            method: 'GET',
            url: '/Reservation/Reservation/GetReservations',
            params: {
                fromDate: dateHelper.dateToString(__self.from),
                toDate: dateHelper.dateToString(__self.to)
            }
        }).success(function (data) {
            __self.reservations.pushAll(data);
        });
    };

    __self.findHour = function (doctorId, date, fromTime, toTime) {
        if (toTime === 0 && fromTime > 0)
            toTime = 1440;
        for (var i = 0; i < __self.days.length; i++) {
            if (dateHelper.isDatesEqual(__self.days[i].date, date)) {
                for (var j = 0; j < __self.days[i].doctors.length; j++) {
                    var doctor = __self.days[i].doctors[j];
                    if (doctor.doctorId === doctorId) {
                        for (var k = 0; k < __self.days[i].doctors[j].hours.length; k++) {
                            var hour = __self.days[i].doctors[j].hours[k];
                            if (hour.from == fromTime && hour.to == toTime) {
                                return hour;
                            }
                        }
                    }
                }
            }
        }
        return null;
    };

    __self.applyReservations = function () {
        for (var l = 0; l < __self.reservations.length; l++) {
            var reservation = __self.reservations[l];
            for (var m = 0; m < reservation.hours.length; m++) {
                var reservationHour = reservation.hours[m];
                var fromTime = timeConverter.convertToMinutes(reservationHour.fromTime);
                var toTime = timeConverter.convertToMinutes(reservationHour.toTime);

                var hour = __self.findHour(reservation.doctorId, dateHelper.parseDate(reservation.date), fromTime, toTime);
                if (hour) {
                    hour.isReserved = true;
                    hour.reservation = {
                        id: reservationHour.id,
                        patientId: reservationHour.patientId,
                        firstName: reservationHour.patientFirstName,
                        lastName: reservationHour.patientLastName,
                        phone: reservationHour.patientPhone,
                        note: reservationHour.note,
                        paymentType: reservationHour.paymentType,
                        createdBy: reservationHour.createdBy
                    };
                }
            }
        }
    };

    __self.removeReservation = function (doctorId, date, fromTime, toTime) {
        var hour = __self.findHour(doctorId, date, fromTime, toTime);
        if (hour) {
            hour.isReserved = false;
            hour.reservation = {};
        }
    };

    __self.addReservation = function (reservation) {
        __self.reservations.clear();
        __self.reservations.push(reservation);
        __self.applyReservations();
    };

    __self.getData = function () {
        var waits = [];
        waits.push(__self.getSchedule());
        waits.push(__self.getAllReservations());
        $q.all(waits).then(__self.applyReservations);
    };

    var currentDate = dateHelper.copyDate(new Date()); //Remove hour offset.
    __self.setWeekByDate(currentDate);
    __self.currentDate = dateHelper.copyDate(__self.from);
    __self.getData();

    var res =
    {
        days: __self.days,

        currentDate: __self.currentDate,

        filterDay: '',

        setDate: __self.setWeekByDate,

        reload: __self.getData,

        removeReservation: __self.removeReservation,

        addReservation: __self.addReservation,

        name: function () {
            return dateHelper.dateToUserString(__self.from) + " - " + dateHelper.dateToUserString(__self.to);
        },

        next: function () {
            __self.from.setDate(__self.from.getDate() + 7);
            __self.to.setDate(__self.to.getDate() + 7);
            res.currentDate.setDate(res.currentDate.getDate() + 7);
            __self.getData();
            res.filterDay = '';
        },

        prev: function () {
            __self.from.setDate(__self.from.getDate() - 7);
            __self.to.setDate(__self.to.getDate() - 7);
            res.currentDate.setDate(res.currentDate.getDate() - 7);
            __self.getData();
            res.filterDay = '';
        },

        doctors: function () {
            var doctors = [];
            for (var i = 0; i < __self.days.length; i++) {
                for (var j = 0; j < __self.days[i].doctors.length; j++) {
                    if (!doctors.some(function (d) { return d.doctorId === __self.days[i].doctors[j].doctorId; })) {
                        doctors.push(__self.days[i].doctors[j]);
                    }
                }
            }
            return doctors;
        }
    }
    return res;
}]);

app.service('reservationChangeListener', function () {
    var __self = this;
    __self.hub = $.connection.reservationHub;

    __self.connect = function (onReservationMade, onReservationRemoved) {
        $.connection.hub.url = "/signalr";
        __self.hub.client.sendReservationMade = onReservationMade;
        __self.hub.client.sendReservationRemoved = onReservationRemoved;
        return $.connection.hub.start();
    };

    __self.getId = function () {
        return $.connection.hub.id;
    }
});

app.controller('registrationController', ['$scope', '$http', 'dateHelper', 'week', '$modal', 'timeConverter', 'reservationChangeListener',
    function ($scope, $http, dateHelper, week, $modal, timeConverter, reservationChangeListener) {
        var __self = this;

        $scope.week = week;

        $scope.now = dateHelper.copyDate(new Date());

        __self.listenForChanges = function () {
            reservationChangeListener.connect(
                //On Add Reservation
                function (doctorId, date, fromTime, toTime, reservationId, patientId, firstName, lastName, note, phone, paymentType, userName) {
                    var reservation = {
                        doctorId: doctorId,
                        date: date,
                        hours: [
                            {
                                id: reservationId,
                                fromTime: fromTime,
                                toTime: toTime,
                                patientId: patientId,
                                patientFirstName: firstName,
                                patientLastName: lastName,
                                note: note,
                                patientPhone: phone,
                                paymentType: paymentType,
                                createdBy: userName
                            }
                        ]
                    };
                    $scope.week.addReservation(reservation);
                    $scope.$apply();
                },
                //On Remove Reservation
                function (doctorId, date, fromTime, toTime) {
                    $scope.week.removeReservation(doctorId, dateHelper.parseDate(date), timeConverter.convertToMinutes(fromTime), timeConverter.convertToMinutes(toTime));
                    $scope.$apply();
                });
        };

        __self.listenForChanges();

        $scope.registerHour = function (doctor, date, hour) {
            __self.showRegistrationDialog(doctor, date, hour);
        };

        $scope.modifyHour = function (doctor, date, hour) {
            __self.showRegistrationDialog(doctor, date, hour);
        };

        __self.showRegistrationDialog = function (doctor, date, hour) {
            $modal.open({
                templateUrl: '/Reservation/Reservation/Reserve',
                controller: 'registrationAddController',
                //size: 'lg',
                resolve: {
                    info: function () {
                        return {
                            doctor: doctor,
                            date: date,
                            hour: hour
                        };
                    }
                }
            });
        };

        $scope.removeReservation = function (id) {
            $http({
                method: 'POST',
                url: '/Reservation/Reservation/removeReservation',
                data: {
                    id: id
                }
            });
        };

        $scope.viewPatient = function (patientId) {
            $modal.open({
                templateUrl: '/Common/Patient/Add',
                controller: 'patientAddController',
                resolve: {
                    id: function () {
                        return patientId;
                    }
                }
            });
        };

        $scope.viewDoctor = function (doctorId) {
            $modal.open({
                templateUrl: '/Common/Doctor/Add',
                controller: 'doctorAddController',
                resolve: {
                    id: function () {
                        return doctorId;
                    }
                }
            });
        };

        $scope.$watch("week.currentDate", function (newVal, oldVal) {
            if (newVal && oldVal && newVal.valueOf() != oldVal.valueOf()) {
                $scope.week.setDate(newVal);
                $scope.week.reload();
            }
        });
    }
]);
