﻿app.filter('nonZeroHour', [function () {
    return function (hours) {
        return _.filter(hours, function (h) {
            return h.to - h.from > 0;
        });
    };
}]);

app.filter('doctorFilter', [function () {
    return function (doctors, doctor) {
        if (!doctor)
            return doctors;
        return _.filter(doctors, function (d) {
            return d.doctorId === doctor.doctorId;
        });
    };
}]);

app.filter('dayDoctorFilter', [function () {
    return function (dates, doctor) {
        if (!doctor)
            return dates;
        return _.filter(dates, function (d) {
            return _.any(d.doctors, function (doc) { return doc.doctorId == doctor.doctorId; });
        });
    };
}]);

app.filter('reserveDateFilter', ['dateHelper', function (dateHelper) {
    return function (dates, filter) {
        if (!filter)
            return dates;
        if (_.all(dates, function (d) { return !dateHelper.isDatesEqual(d.date, dateHelper.parseDate(filter)); }))
            return dates;
        return _.filter(dates, function (d) {
            return dateHelper.isDatesEqual(d.date, dateHelper.parseDate(filter));
        });
    };
}]);

app.directive('vmQtip', ['timeConverter', 'dateHelper', function (timeConverter, dateHelper) {
    return {
        link: function (scope, elm, attrs) {
            var setTip = function () {

                var content = scope.doctor.title + " " + scope.doctor.firstName + " " + scope.doctor.lastName + "\n";
                if (scope.hour.work === 0)
                    content += 'Неработи';
                else {
                    if (scope.hour.isReserved) {
                        content += "Запазен";
                    } else {
                        if (scope.hour.isnzok)
                            content += 'Работи по здравна каса.';
                        else
                            content += 'Работи.';
                    }
                }
                content += '\nДата: ' + dateHelper.dateToUserString(scope.day.date);
                content += "\nЧас: " + timeConverter.convertToHours(scope.hour.from) + " - " + timeConverter.convertToHours(scope.hour.to);
                if (scope.hour.isReserved) {
                    content += '\nПациент: ' + scope.hour.reservation.firstName + ' ' + scope.hour.reservation.lastName;
                    content += '\nТелефон: ' + scope.hour.reservation.phone;
                    if (scope.hour.reservation.note) {
                        content += '\nЗабележка: ' + scope.hour.reservation.note;
                    }
                    content += '\nНачин за плащане: ' + scope.hour.reservation.paymentType;
                    content += '\nСъздаден от: ' + scope.hour.reservation.createdBy;
                }
                attrs.$set('data-title', content);
            };

            setTip();

            scope.$watch('hour.isReserved', function () {
                setTip();
            });
        }
    };
}]);