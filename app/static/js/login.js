﻿var loginApp = angular.module("loginApp", ['ui.translate', 'ui.med']);

loginApp.controller('loginController', [
    '$scope', 'translationService', function ($scope, translation) {
        $scope.translation = translation;
        translation.applyTranslation();
    }
]);