﻿var app = angular.module("app", ['ngCookies', 'ui.bootstrap', 'ui.med', 'common.cntrl', 'ui.translate']);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.cache = false;
}])
    .run(['$http', '$cookies', function ($http, $cookies) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
    }]);

app.run(['$rootScope', 'translationService',
    function ($rootScope, translation) {
        $rootScope.translation = translation;
        translation.applyTranslation();
    }
]);

app.controller('rootController', [
    '$scope', function ($scope) {
        $scope.init = function (currentUserId) {
            $scope.currentUserId = currentUserId;
        }
    }
]);