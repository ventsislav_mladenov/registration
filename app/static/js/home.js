﻿app.controller('homeController', [
    "$scope", '$http', function ($scope, $http) {
        var __self = this;
        $scope.messages = [];
        __self.loadMessages = function () {
            $http(
            {
                method: 'GET',
                url: '/Home/GetMessages'
            }).success(function (res) {
                $scope.messages.clear();
                $scope.messages.pushAll(res);
            });
        };

        __self.loadMessages();

        $scope.read = function (id) {
            $http(
            {
                method: 'POST',
                url: '/Home/ReadMessage',
                data: {
                    id: id
                }
            }).success(function (res) {
                __self.loadMessages();
            });
        };
    }
]);