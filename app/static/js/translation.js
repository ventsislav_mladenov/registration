﻿angular.module('ui.translate', [])
    .service('translationService', ['$http',
        function ($http) {
            var _self = this;

            _self.defaultLang = 'en';
            _self.availableLangs = ['en', 'bg'];
            var getLang = function () {
                if (document.cookie.indexOf('lang=') > -1) {
                    var lang = document.cookie.replace(/(?:(?:^|.*;\s*)lang\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    if (lang && _self.availableLangs.indexOf(lang) > -1) {
                        return lang;
                    }
                }
                return _self.defaultLang;
            };
            var currentLang = getLang();

            var setLang = function (lang) {
                var now = new Date();
                now.setFullYear(now.getFullYear() + 1);
                document.cookie = 'lang=' + lang + ';path=/;expires=' + now.toGMTString();
                currentLang = lang;
            };

            var loadLanguage = function (lng) {
                if (!lng)
                    lng = _self.defaultLang;

                $http({
                    method: 'GET',
                    url: '/static/js/i18n/' + lng + '.js'
                }).success(function (texts) {
                    eval(texts);
                    _self.texts = window.translation.texts;
                    setLang(lng);
                });
            };

            if (window.translation)
                _self.texts = window.translation.texts;
            else
                _self.texts = null;

            _self.setLanguage = function (lng) {
                if (currentLang != lng) {
                    loadLanguage(lng);
                };
            };

            _self.applyTranslation = function () {
                var lang = getLang();
                if (currentLang != lang || !_self.texts) {
                    loadLanguage(lang);
                }
            };
        }
    ]);