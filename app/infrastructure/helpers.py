import app


def check_relation(obj, relation_name):
    try:
        return hasattr(obj, relation_name)
    except app.models.models.DoesNotExist:
        return False
