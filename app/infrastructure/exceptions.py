class MissingParameter(Exception):
    """ Exception when there is a missing parameter in url """
    def __init__(self, parameter_name):
        self.message = 'Missing parameter: ' + parameter_name

    def __str__(self):
        repr(self.message)


class WrongParameterValue(Exception):
    """ Exception when parameter value is wrong """
    def __init__(self, parameter_name):
        self.message = 'Wrong parameter value! Parameter name: ' + parameter_name

    def __str__(self):
        repr(self.message)
